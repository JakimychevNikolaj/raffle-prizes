<?php

use yii\db\Migration;

/**
 * Handles the creation of table `innerBank`.
 */
class m181118_192854_create_innerBank_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('innerBank', [
            'id' => $this->primaryKey(),
            'moneyLeft' => $this->integer()->notNull(),
        ]);
		$this->insert('innerBank', array(
			'moneyLeft' => 1000000
		));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('innerBank');
    }
}
