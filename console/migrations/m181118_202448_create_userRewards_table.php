<?php

use yii\db\Migration;

/**
 * Handles the creation of table `userRewards`.
 */
class m181118_202448_create_userRewards_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('userRewards', [
            'id' => $this->primaryKey(),
			'userId' => $this->integer()->notNull(),
			'itemId' => $this->integer(),
			'points' => $this->integer(),
			'money' => $this->integer(),
			'status' => $this->smallInteger()->defaultValue(0),
        ]);
		$this->addForeignKey(
			'userRewards-user-1',
			'userRewards',
			'userId',
			'user',
			'id',
			'CASCADE'
		);
		$this->addForeignKey(
			'userRewards-items-1',
			'userRewards',
			'itemId',
			'items',
			'id',
			'SET NULL'
		);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
		$this->dropForeignKey('userRewards-user-1', 'userRewards');
		$this->dropForeignKey('userRewards-items-1', 'userRewards');
        $this->dropTable('userRewards');
    }
}
