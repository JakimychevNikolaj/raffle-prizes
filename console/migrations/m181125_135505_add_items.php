<?php

use yii\db\Migration;

/**
 * Class m181125_135505_add_items
 */
class m181125_135505_add_items extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
	{
		$this->insert('items', [
			'name' => 'airpods',
			'price' => '12000',
			'amount' => '10',
		]);
		$this->insert('items', [
			'name' => 'ipad 3',
			'price' => '30000',
			'amount' => '5',
		]);
		$this->insert('items', [
			'name' => 'iphone 7',
			'price' => '28000',
			'amount' => '5',
		]);
		$this->insert('items', [
			'name' => 'iphone 5',
			'price' => '10000',
			'amount' => '15',
		]);
		$this->insert('items', [
			'name' => 'GeForce GTX 1080 Ti',
			'price' => '80000',
			'amount' => '1',
		]);
		$this->insert('items', [
			'name' => 'OnePlus 5',
			'price' => '34000',
			'amount' => '3',
		]);
		$this->insert('items', [
			'name' => 'Phone Case',
			'price' => '200',
			'amount' => '200',
		]);
	}

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
		return	$this->delete('items');
    }
}
