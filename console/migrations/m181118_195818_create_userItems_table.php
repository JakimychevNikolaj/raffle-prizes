<?php

use yii\db\Migration;

/**
 * Handles the creation of table `userItems`.
 */
class m181118_195818_create_userItems_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('userItems', [
            'id' => $this->primaryKey(),
            'userId' => $this->integer()->notNull(),
            'itemId' => $this->integer()->notNull(),
            'status' => $this->smallInteger()->defaultValue(0),
        ]);
		$this->addForeignKey(
			'userItems-user-1',
			'userItems',
			'userId',
			'user',
			'id',
			'CASCADE'
		);
		$this->addForeignKey(
			'userItems-items-1',
			'userItems',
			'itemId',
			'items',
			'id',
			'CASCADE'
		);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
		$this->dropForeignKey('userItems-user-1', 'userItems');
		$this->dropForeignKey('userItems-items-1', 'userItems');
        $this->dropTable('userItems');
    }
}
