<?php

use yii\db\Migration;

/**
 * Handles the creation of table `userBank`.
 */
class m181118_201500_create_userBank_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('userBank', [
            'id' => $this->primaryKey(),
			'userId' => $this->integer()->notNull(),
			'amount' => $this->integer()->defaultValue(0),
			'cashLimit' => $this->integer()->defaultValue(50000),
			'withdrawalLimit' => $this->integer()->defaultValue(5000),
        ]);
		$this->addForeignKey(
			'userBank-user-1',
			'userBank',
			'userId',
			'user',
			'id',
			'CASCADE'
		);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
		$this->dropForeignKey('userBank-user-1', 'userBank');
        $this->dropTable('userBank');
    }
}
