<?php

use yii\db\Migration;

/**
 * Handles the creation of table `userPoints`.
 */
class m181118_200534_create_userPoints_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
	{
		$this->createTable('userPoints', [
			'id' => $this->primaryKey(),
			'userId' => $this->integer()->notNull(),
			'pointsAmount' => $this->integer()->defaultValue(0),
			'pointsLimit' => $this->integer()->defaultValue(1000000)
		]);
		$this->addForeignKey(
			'userPoints-user-1',
			'userPoints',
			'userId',
			'user',
			'id',
			'CASCADE'
		);
	}

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
		$this->dropForeignKey('userPoints-user-1', 'userPoints');
        $this->dropTable('userPoints');
    }
}
