<?php

namespace console\controllers;

use yii\console\Controller;
use common\models\db\UserRewards;
use common\models\db\UserBank;
use common\models\db\UserPoints;
use common\models\User;

/**
 * Class PrizeController
 */
class PrizeController extends Controller
{
	/**
	 * sends rewards to users
     * @param string $amount
	 * @return void
	 */
	public function actionSend($amount)
	{
		foreach (User::find()->each() as $user) {
			$userBank = UserBank::get($user->id);
			$userPoints = UserPoints::get($user->id);
			$points = 0;
			$money = 0;

			$rewardQuery = $user
				->getUserRewards()
				->andWhere(['<>', 'status', 2])
				->andWhere(['itemId' => null])
				->limit($amount);

			foreach ($rewardQuery->each() as $reward) {
				$points += $reward->points;
				$money += $reward->money;
				$reward->status = UserRewards::STATUS_AVAILABLE;
				$reward->save();
			}

			$userBank->updateMoney($money);
			$userPoints->updatePoints($points);
		}
	}
	
}
