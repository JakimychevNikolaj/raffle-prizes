<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "items".
 *
 * @property int $id
 * @property string $name
 * @property int $price
 * @property int $amount
 * @property int $status
 *
 * @property UserItems[] $userItems
 * @property UserRewards[] $userRewards
 */
class Items extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'items';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['price', 'amount', 'status'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'price' => 'Price',
            'amount' => 'Amount',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserItems()
    {
        return $this->hasMany(UserItems::className(), ['itemId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserRewards()
    {
        return $this->hasMany(UserRewards::className(), ['itemId' => 'id']);
    }

	public static function rollItem() 
	{
		$items = static::find()->select(['amount'])->andWhere(['>', 'amount', 0])->indexBy('id')->asArray()->column();
		$array = [];
		$start = 0;
		foreach ($items as $id => $amount) {
			$array = array_merge($array, array_fill($start, $amount, $id));
			$start += $amount;
		}
		shuffle($array);
		return $array[array_rand($array)];
	}

	public function decreaseAmount() 
	{
		$this->amount -= 1;
		return $this->save();
	}
}
