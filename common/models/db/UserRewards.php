<?php

namespace common\models\db;

use Yii;
use common\models\User;
use common\models\db\Items;
use common\models\db\InnerBank;

/**
 * This is the model class for table "userRewards".
 *
 * @property int $id
 * @property int $userId
 * @property int $itemId
 * @property int $points
 * @property int $money
 * @property int $status
 *
 * @property Items $item
 * @property User $user
 */
class UserRewards extends \yii\db\ActiveRecord
{
	const STATUS_NOT_SENDED = 0;
	const STATUS_SENDED = 1;
	const STATUS_AVAILABLE = 2;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'userRewards';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['userId'], 'required'],
            [['userId', 'itemId', 'points', 'money', 'status'], 'integer'],
            [['itemId'], 'exist', 'skipOnError' => true, 'targetClass' => Items::className(), 'targetAttribute' => ['itemId' => 'id']],
            [['userId'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['userId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'userId' => 'User ID',
            'itemId' => 'Item ID',
            'points' => 'Points',
            'money' => 'Money',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItem()
    {
        return $this->hasOne(Items::className(), ['id' => 'itemId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'userId']);
    }
	
    /**
     * @return integer
     */
	public function getPointsAmount()
	{
		$user = User::getUser();
		$result = static::find()->select(['points'])->andWhere(['userId' => $user->id])->sum('points');
		return ($result) ? $result :  0;
	}
	
    /**
     * @return integer
     */
	public function getMoneyAmount()
	{
		$user = User::getUser();
		$result = static::find()->select(['money'])->andWhere(['userId' => $user->id])->sum('money');
		return ($result) ? $result :  0;
	}

	public static function create($type, $value)
	{
		$user = User::getUser();
		$userRewards = new UserRewards();
		$userRewards->userId = $user->id;

		if ($type == 0) {
			$userRewards->points = $value;
		} elseif ($type == 1) {
			$innerBank = InnerBank::find()->one();
			$innerBank->decreaseAmount($value);
			$userRewards->money = $value;
		} elseif ($type == 2) {
			$item = Items::findOne($value);
			$item->decreaseAmount();
			$userRewards->itemId = $value;
		}

		return $userRewards->save();
	}
}
