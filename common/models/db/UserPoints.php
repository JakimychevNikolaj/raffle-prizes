<?php

namespace common\models\db;

use Yii;
use common\models\User;

/**
 * This is the model class for table "userPoints".
 *
 * @property int $id
 * @property int $userId
 * @property int $pointsAmount
 * @property int $pointsLimit
 *
 * @property User $user
 */
class UserPoints extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'userPoints';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['userId'], 'required'],
            [['userId', 'pointsAmount', 'pointsLimit'], 'integer'],
            [['userId'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['userId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'userId' => 'User ID',
            'pointsAmount' => 'Points Amount',
            'pointsLimit' => 'Points Limit',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'userId']);
    }
	
	public function get($userId = null)
	{
		$user = User::getUser();
		$userId = $userId ?? $user->id;
		$userPoints = static::findOne(['userId' => $userId]);
		if (!$userPoints) {
			$userPoints = new UserPoints();
			$userPoints->userId = $user->id;
			$userPoints->save();
		}
		return $userPoints;
	}

	public function updatePoints(int $points) {
		if (($this->pointsAmount + $points) > $this->pointsLimit) {
			return false;
		}
		$this->pointsAmount += $points;
		return $this->save();
	}
}
