<?php

namespace common\models\db;

use Yii;
use common\models\User;

/**
 * This is the model class for table "userBank".
 *
 * @property int $id
 * @property int $userId
 * @property int $amount
 * @property int $cashLimit
 * @property int $withdrawalLimit
 *
 * @property User $user
 */
class UserBank extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'userBank';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['userId'], 'required'],
            [['userId', 'amount', 'cashLimit', 'withdrawalLimit'], 'integer'],
            [['userId'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['userId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'userId' => 'User ID',
            'amount' => 'Amount',
            'cashLimit' => 'Cash Limit',
            'withdrawalLimit' => 'Withdrawal Limit',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'userId']);
    }

	public function get($userId = null)
	{
		$user = User::getUser();
		$userId = $userId ?? $user->id;
		$userBank = static::findOne(['userId' => $userId]);
		if (!$userBank) {
			$userBank = new UserBank();
			$userBank->userId = $user->id;
			$userBank->save();
		}
		return $userBank;
	}

	public function updateMoney(int $money) 
	{
		if (($this->amount + $money) > $this->cashLimit) {
			return false;
		}
		$this->amount += $money;
		return $this->save();
	}
}
