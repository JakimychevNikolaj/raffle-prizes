<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "innerBank".
 *
 * @property int $id
 * @property int $moneyLeft
 */
class InnerBank extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'innerBank';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['moneyLeft'], 'required'],
            [['moneyLeft'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'moneyLeft' => 'Money Left',
        ];
    }

	public function decreaseAmount($value)
	{
		$this->moneyLeft -= $value;
		return $this->save();
	}
}
