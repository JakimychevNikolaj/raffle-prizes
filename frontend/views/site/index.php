<?php
use yii\helpers\Html;
use yii\helpers\Url;
use common\models\db\UserBank;
use common\models\db\UserPoints;
use common\models\db\UserRewards;

//$this->registerJsFile(Yii::$app->request->baseUrl.'/js/prize.js',['depends' => [\yii\web\JqueryAsset::className()]]);

/* @var $this yii\web\View */

echo Html::button('Roll', ['id' => 'btn', 'class' => 'btn btn-primary']);
?>
<?= Html::button('Transfer to points', ['id' => 'transfer-btn', 'class' => 'hidden btn btn-warning']);?>
<span id='btn-group' class='hidden'> 
<?php
	echo Html::button('Get prize', ['id' => 'get-prize', 'class' => 'btn btn-success']);
	echo Html::button('Cancel', ['id' => 'cancel', 'class' => 'btn btn-danger']);
?>
</span>

<div class='sidebar-info'> 
	<div class='user-bank'> 
		<div>У вас на счету <?=UserRewards::getMoneyAmount(); ?> руб.</div>
		<div>Вам доступно <?=UserBank::get()->amount; ?> руб.</div>
	</div>
	<div class='user-points'> 
		<div>У вас <?=UserRewards::getPointsAmount(); ?> баллов.</div>
		<div>Вам доступно <?=UserPoints::get()->pointsAmount; ?> баллов.</div>
	</div>
</div>


<div id='result' class=''> 
</div>
