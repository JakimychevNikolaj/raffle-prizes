<?php
namespace frontend\controllers;
use common\models\db\Items;
use common\models\db\UserRewards;
use yii\filters\VerbFilter;

class PrizeController extends PrivateController
{
	const ROLL_RESULT_POINTS = 0;
	const ROLL_RESULT_MONEY = 1;
	const ROLL_RESULT_ITEM = 2;
	
    /**
     * @inheritdoc
     */
	public function runAction($id, $params = [])
	{
		$params = \yii\helpers\BaseArrayHelper::merge(\Yii::$app->getRequest()->getBodyParams(), $params);
		return parent::runAction($id, $params);
	}

    /**
     * @inheritdoc
     */
	public function behaviors()
    {
        $behaviors = parent::behaviors();
		$behaviors['verbs'] = [
			'class' => VerbFilter::className(),
			'actions' => [
				'accept' => ['POST'],
			],
		];
		return $behaviors;
    }

	//default chances: 60% points, 30% money, 10% item
	public function actionRoll() 
	{
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$rand = rand(0, 300);
		if ($rand <= 180) {
			return [
				'type' => static::ROLL_RESULT_POINTS, 
				'value' => $value = rand(100, 4000), 
				'message' => $value . ' баллов'
			];
		} elseif ($rand <= 270) {
			return [
				'type' => static::ROLL_RESULT_MONEY, 
				'value' => $value = rand(100, 3000), 
				'message' => $value . ' рублей'
			];
		} elseif ($rand <= 300) {
			$itemId = Items::rollItem();
			$item = Items::findOne($itemId);
			return [
				'type' => static::ROLL_RESULT_ITEM, 
				'value' => $itemId, 
				'message' => $item->name
			];
		}
	}

	public function actionAccept($type, $value) 
	{
		return UserRewards::create($type, $value);
	}
}
