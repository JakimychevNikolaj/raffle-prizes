var type = 0;
var value = 0;


$('#btn').click(function() {
	$.ajax({
		url: 'index.php?r=prize%2Froll',
		type: 'POST',
		success: function (data, textStatus, jqXHR) {
			type = data.type;
			value = data.value;
			toggleButtons();
			$('#result').append('<p> Поздравляем, вы выиграли приз: ' + data.message + '</p>');
			if (type == 1) {
				$('#transfer-btn').toggleClass('hidden');
			}
		},
	});
});

function toggleButtons() {
	$('#btn').attr('disabled', function(_, attr){ return !attr});
	$('#btn-group').toggleClass('hidden');
}

$('#btn-group').on('click', 'button', function(evt) {
	toggleButtons();
	if (!$('#transfer-btn').hasClass('hidden')) {
		$('#transfer-btn').toggleClass('hidden');
	}
});

$('#get-prize').click(function() {
	$.ajax({
		url: 'index.php?r=prize%2Faccept',
		type: 'POST',
		data: {
			'type': type,
			'value': value
		},
	});
});

$('#transfer-btn').click(function() {
	$.ajax({
		url: 'index.php?r=prize%2Faccept',
		type: 'POST',
		data: {
			'type': 0,
			'value': value * 5
		},
	});
	$('#transfer-btn').toggleClass('hidden');
	toggleButtons();
});
